from dataclasses import asdict, dataclass
from enum import Enum
from typing import Any, Dict, List

import numpy as np
from pydantic import BaseModel, validator


class InputData(BaseModel):
    """Defines input data as expected from external services."""

    values: List[float]

    class Config:

        schema_extra = {"example": {"values": [1.465, 2.43, 0.78, 2.15]}}


class FloatTensor(BaseModel):
    """Defines expected data by the inference service."""

    values: np.ndarray

    @validator("values", pre=True)
    def validate_array(cls, value: List[float]) -> np.ndarray:
        return np.array(value, dtype=np.float32)

    class Config:
        arbitrary_types_allowed = True


class ClassificationResult(BaseModel):
    label: int
    probabilities: Dict[int, float]


class ClassificationResults(BaseModel):
    results: List[ClassificationResult]


class ResultFormat(str, Enum):
    dict = "dict"
    tuple = "tuple"
    result = "result"
    list = "list"


@dataclass
class Result:
    label: int
    probabilities: Dict[int, float]

    def to_dict(self) -> Dict[str, Any]:
        return asdict(self)


@dataclass
class Results:
    labels: List[int]
    probabilities: List[Dict[int, float]]

    def __post_init__(self) -> None:
        self._zipped_values = list(zip(self.labels, self.probabilities))

    def to_list(self, format: ResultFormat = ResultFormat.dict) -> List[Any]:
        if format == ResultFormat.dict:
            return [asdict(Result(*result)) for result in self._zipped_values]
        if format == ResultFormat.tuple:
            return self._zipped_values
        if format == ResultFormat.result:
            return [Result(*result) for result in self._zipped_values]
        if format == ResultFormat.list:
            return list(map(lambda x: list(x), self._zipped_values))
        raise ValueError(f"Unknown format: {format}")

    def to_dict(self) -> Dict[str, Any]:
        return asdict(self)


class Response(BaseModel):
    label: int
    probabilities: Dict[int, float]

    class Config:
        schema_extra = {
            "example": {
                "label": 0,
                "probabilities": {
                    "0": 0.969999372959137,
                    "1": 0.019999999552965164,
                    "2": 0.009999999776482582,
                },
            }
        }
