"""Settings for S3 model storage"""
from pydantic import BaseSettings


class S3StorageSettings(BaseSettings):
    endpoint: str = "localhost:9000"
    bucket: str = "models"
    access_key: str = "minioadmin"
    secret_key: str = "minioadmin"
    secure: bool = False

    class Config:
        env_prefix = "s3_"
