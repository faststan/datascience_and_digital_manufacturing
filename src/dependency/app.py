"""A simple application built using Model As Dependency pattern.

This application is installed automatically when installing the project.
Try using it with the following command:

$ demo-ds-manuf 1 2 3 4

"""
from typing import List

from loguru import logger
from typer import Argument, Typer

from .sessions import InferenceSessionManager

# Typer framework is used to create a command line application
app = Typer(add_completion=False)

# An InfernceSessionManager is a useful tool to load model and perform prediction.
# It is developed by Capgemini.
manager = InferenceSessionManager()


# Let's define a command for our CLI application
@app.command(name="predict")
def predict(
    input: List[float] = Argument(..., help="A list of float values separated by space")
) -> None:
    """Perform a prediction and print the result to the console."""
    # Create a session using a specific model
    with manager.new_session(
        "demo_session", url="https://s3.per-grenoble.dev/models/rf_iris.onnx",
    ) as session:
        # Perform a prediction using the session
        results = session.predict(input)
    # Log the results to the console
    logger.info(f"New result: {results}")
