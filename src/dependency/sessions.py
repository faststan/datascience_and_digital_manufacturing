from contextlib import contextmanager
from pathlib import Path
from typing import Dict, Iterator, List, Optional

import httpx
import numpy as np
import onnxruntime as rt
from loguru import logger
from minio import Minio

from .interfaces import FloatTensor, Results
from .s3 import S3StorageSettings


class InferenceSessionManager:
    def __init__(self) -> None:
        self.sessions: Dict[str, rt.InferenceSession] = {}
        self.settings = S3StorageSettings().dict()
        self.bucket = self.settings.pop("bucket")
        self.minio = Minio(**self.settings)

    @staticmethod
    def __load_model_from_filepath__(path: str) -> bytes:
        """Load a model from given filepath."""
        filepath = Path(path)
        if not filepath.is_file():
            raise ValueError(f"File {path} does not exist.")
        logger.debug(f"Loading model from file: {filepath}")
        return filepath.read_bytes()

    @staticmethod
    def __load_model_from_url__(url: str) -> bytes:
        """Load a model from given URL by performing a GET request."""
        logger.debug(f"Loading model from url: {url}")
        response = httpx.get(url)
        response.raise_for_status()
        return response.content

    def __load_model_from_s3__(self, path: str) -> bytes:
        """Load a model from given S3 path."""
        logger.debug(f"Loading model from s3 path: {path}")
        resp = self.minio.get_object(self.bucket, path)
        return resp.data

    def __load_model__(
        self, path: Optional[str], s3_path: Optional[str], url: Optional[str],
    ) -> bytes:
        if path:
            return self.__load_model_from_filepath__(path)
        if s3_path:
            return self.__load_model_from_s3__(s3_path)
        if url:
            return self.__load_model_from_url__(url)
        raise ValueError(
            "You must provide either a local path (path), an S3 path (s3_path), an URL (url)."
        )

    def create_session(
        self,
        name: str,
        *,
        path: Optional[str] = None,
        s3_path: Optional[str] = None,
        url: Optional[str] = None,
        data: Optional[bytes] = None,
    ) -> rt.InferenceSession:
        if data:
            _model = data
        else:
            _model = self.__load_model__(path=path, s3_path=s3_path, url=url)
        session = rt.InferenceSession(_model)
        input_name = session.get_inputs()[0].name
        label_name = session.get_outputs()[0].name
        pred_name = session.get_outputs()[1].name

        def predict(input_data: List[float]) -> Results:
            X = FloatTensor(values=input_data)
            values = np.array([X.values])
            labels, predictions = session.run(
                output_names=[label_name, pred_name], input_feed={input_name: values}
            )
            return Results(labels, predictions)

        session.predict = predict

        self.sessions[name] = session

        return session

    @contextmanager
    def new_session(
        self,
        name: str,
        *,
        path: Optional[str] = None,
        s3_path: Optional[str] = None,
        url: Optional[str] = None,
        data: Optional[bytes] = None,
    ) -> Iterator[rt.InferenceSession]:
        if all([elem is None for elem in [path, s3_path, url, data]]):
            raise ValueError(
                "You must provide either a local path, an S3 path, an URL or raw model as bytes when creating a session."
            )
        session = self.sessions.get(name) or self.create_session(
            name=name, path=path, s3_path=s3_path, url=url, data=data
        )
        yield session
        del session
