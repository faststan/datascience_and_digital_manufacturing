"""A simple application built using Model As Dependency pattern.

This application is installed automatically when installing the project.
Try using it with the following command:

$ demo-ds-manuf 1 2 3 4

"""
from typing import Any, Dict

from fastapi import FastAPI

from dependency.interfaces import InputData, Response, ResultFormat
from dependency.sessions import InferenceSessionManager

# FastAPI framework is used to create a REST API (web service)
app = FastAPI(title="Demo Datascience & Digital Manuf")

# An InfernceSessionManager is a useful tool to load model and perform prediction.
# It is developed by Capgemini.
manager = InferenceSessionManager()


# Let's define an endpoint for our web service
@app.post("/predict", response_model=Response, tags=["Inference"])
def predict(json_body: InputData) -> Dict[str, Any]:
    """Perform a prediction and return a result in JSON."""
    # Create a session using a specific model
    with manager.new_session(
        "demo_session", url="https://s3.per-grenoble.dev/models/rf_iris.onnx",
    ) as session:
        # Perform a prediction using the session and values found in JSON body
        results = session.predict(json_body.values)
    # Let's take the first prediction result (we did only 1 anyway)
    result = results.to_list(format=ResultFormat.result)[0]
    # Return a response holding prediction result
    return result.to_dict()
