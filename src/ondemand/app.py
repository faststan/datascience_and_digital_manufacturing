"""A simple application built using Model As Dependency pattern.

This application is installed automatically when installing the project.
Try using it with the following command:

$ demo-ds-manuf 1 2 3 4

"""
from typing import Any, Dict

from fastapi import FastAPI, HTTPException
from faststan.nats import FastNATS

from dependency.interfaces import InputData, Response

# FastAPI framework is used to create a REST API (web service)
app = FastAPI(title="Demo Datascience & Digital Manuf")

nats_client = FastNATS()


@app.on_event("startup")
async def connect_nats() -> None:
    await nats_client.connect()


# Let's define an endpoint for our web service
@app.post("/predict", response_model=Response, tags=["Inference"])
async def predict(json_body: InputData) -> Dict[str, Any]:
    """Let the worker perform the prediction."""
    response = await nats_client.request_model("predictions_service", json_body)
    if response.status:
        return response.result
    raise HTTPException(status_code=500, detail=response.error)
