"""Machine Learning worker."""
from fastlogger import setup_logger_from_env

from dependency.interfaces import InputData, Response, ResultFormat
from dependency.sessions import InferenceSessionManager

# An InfernceSessionManager is a useful tool to load model and perform prediction.
# It is developed by Capgemini.
manager = InferenceSessionManager()

# Setup the application logger
setup_logger_from_env()


def on_prediction_request(json_body: InputData) -> Response:
    """Callback that will be executed on each prediction request."""
    # Create a session using a specific model
    with manager.new_session(
        "demo_session", url="https://s3.per-grenoble.dev/models/rf_iris.onnx",
    ) as session:
        # Perform a prediction using the session and values found in JSON body
        results = session.predict(json_body.values)
    # Let's take the first prediction result (we did only 1 anyway)
    result = results.to_list(format=ResultFormat.result)[0]
    # Return a response holding prediction result
    return Response(**result.to_dict())
