"""Machine Learning worker."""
from faststan.models import Subject
from loguru import logger

from dependency.interfaces import InputData, Response, ResultFormat
from dependency.sessions import InferenceSessionManager

# An InfernceSessionManager is a useful tool to load model and perform prediction.
# It is developed by Capgemini.
manager = InferenceSessionManager()


def on_prediction_request(json_body: InputData, subject: Subject) -> Response:
    """Callback that will be executed on each prediction request."""
    logger.debug(f"Received message on subject: {subject}")
    model_name = ".".join(subject.split(".")[-2:])
    # Create a session using a specific model
    with manager.new_session(
        model_name, url=f"https://s3.per-grenoble.dev/models/{model_name}",
    ) as session:
        # Perform a prediction using the session and values found in JSON body
        results = session.predict(json_body.values)
    # Let's take the first prediction result (we did only 1 anyway)
    result = results.to_list(format=ResultFormat.result)[0]
    # Return a response holding prediction result
    return Response(**result.to_dict())
