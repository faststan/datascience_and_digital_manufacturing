""" A dummy conftest to illustrate pytest configuration """

from pytest import fixture

from datascience_and_digital_manufacturing import __version__


@fixture
def version() -> str:
    """ Return the current version of the datascience_and_digital_manufacturing package """
    return __version__
